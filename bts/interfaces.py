# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import ldap, sys, os, re, commands

###############################################################################
# UserTags stuff

import SOAPpy

_usertags = {}
def _parseUserTags(cnf):
    url = 'http://bugs.debian.org/cgi-bin/soap.cgi'
    namespace = 'Debbugs/SOAP'
    server = SOAPpy.SOAPProxy(url, namespace)
    map = server.get_usertag(cnf.get('general', 'user'));
    for tag, v in map._asdict().iteritems():
        for b in v:
            b = str(b)
            if b in _usertags:
                _usertags[b].append(tag)
            else:
                _usertags[b] = [tag]

###############################################################################
# Reports Stuff

class BtsReport:
    def __init__(self, id):
        self.id = id
        if id in _usertags:
            self.usertags = _usertags[id]
        else:
            self.usertags = []

        self.forward = None
        self.merge   = None

        if len(self.fwdTo) is 1:
            self.forward = self.fwdTo[0]
            self.merge   = None

        if len(self.fwdTo) is 2 and self.fwdTo[1].startswith('merged-upstream: '):
            self.forward = self.fwdTo[0].strip()
            self.merge   = self.fwdTo[1][len('merged-upstream: '):].strip()

    def package(self):    pass
    def srcPackage(self): pass

    def _usertagValue(self, key):
        prefix = key + '-'
        for t in self.usertags:
            if t.startswith(prefix):
                return t[len(prefix):]
        return None

    def remoteStatus(self):
        return self._usertagValue('status')

    def remoteResolution(self):
        return self._usertagValue('resolution')

class BtsLdapReport(BtsReport):
    def __init__(self, id, ldapdata):
        self.tags       = self._get('debbugsTag', ldapdata)
        self.fwdTo      = self._get('debbugsForwardedTo', ldapdata)
        self.package    = ldapdata['debbugsPackage'][0]
        self.srcpackage = ldapdata['debbugsSourcePackage'][0]
        self.done       = ldapdata['debbugsState'][0].lower() == 'done'
        BtsReport.__init__(self, id)

    def _get(self, idx, data):
        if idx in data: return data[idx]
        return []

class BtsSpoolReport(BtsReport):
    def __init__(self, id, spooldata):
        self._data = spooldata

        if 'Tags' in spooldata:
            self.tags = spooldata['Tags'].split()
        else:
            self.tags = []

        if 'Forwarded-To' in spooldata:
            self.fwdTo = [x.strip() for x in spooldata['Forwarded-To'].split(', ')]
        else:
            self.fwdTo = []

        self.package    = spooldata['Package']
        self.srcpackage = spooldata['Source']
        self.done       = 'Done' in spooldata
        BtsReport.__init__(self, id)


###############################################################################
# BTS Interfaces stuff

class _BtsLdapInterface:
    dn = "dc=current,dc=bugs,dc=debian,dc=org"

    def __init__(self, ldapurl):
        self.l = ldap.initialize(ldapurl)
        self.l.simple_bind_s()

    def search(self, filter):
        return [i[1] for i in self.l.search_s(self.dn, ldap.SCOPE_ONELEVEL, filter)]

    def getReportOfBzBug(self, bzurl, nb):
        url = bzurl
        filter = "(|(debbugsForwardedTo=%s/%s)(debbugsForwardedTo=%s/show_bug.cgi?id=%s))"
        filter %= (url, nb, url, nb)

        l = self.search(filter)
        if len(l) is 0:
            return None
        else:
            return BtsLdapReport(l[0]['debbugsID'][0], l[0])

    def getReport(self, nb):
        l = self.search('(debbugsID=%s)' % (nb))
        if len(l) is 0:
            return None
        else:
            return BtsLdapReport(nb, l[0])

    def getReportsByFwd(self, *fwds):
        filter = '(|'+''.join(['(debbugsForwardedTo='+s+')' for s in fwds])+')'
        l = self.search(filter)
        return [BtsLdapReport(x['debbugsId'][0], x) for x in l]

class _BtsSpoolInterface:
    def __init__(self, spooldir):
        self._spool = spooldir
        self._map   = {}
        f = file(os.path.join(self._spool, "sources"))
        for l in f.readlines():
            pkg, _, srcpkg = l.split()
            self._map[pkg] = srcpkg
        f.close()

    def _parsefile(self, id):
        f = file(os.path.join(self._spool, id[-2:], id+'.summary'))
        data = {}
        for l in f.readlines():
            k, v = l.split(': ', 1)
            data[k] = v.strip()
        f.close()

        pkg = data['Package'] 
        if pkg in self._map:
            data['Source'] = self._map[pkg]
        else:
            data['Source'] = pkg

        return data

    def getReport(self, bugid):
        try:
            return BtsSpoolReport(bugid, self._parsefile(bugid))
        except IOError:
            return None

    def getReportsByFwd(self, *fwds):
        reg = '|'.join([re.escape(s) for s in fwds])
        cmd = "grep -E '%s' %s/index.fwd" % (reg, self._spool)
        return [self.getReport(l.split(': ', 1)[0]) \
                for l in commands.getoutput(cmd).splitlines()]

###############################################################################
# factory

def BtsInterface(cnf):
    if cnf.get('general', 'spool') is not None:
        _parseUserTags(cnf)
        return _BtsSpoolInterface(cnf.get('general', 'spool'))
    if cnf.get('general', 'ldap') is not None:
        _parseUserTags(cnf)
        return _BtsLdapInterface(cnf.get('general', 'ldap'))
    return None

