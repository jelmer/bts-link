# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#   2015 Sandro Tosi <morph@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

"""Managing configuration file"""

from __future__ import with_statement
import os, re, stat, time, urllib
from sender import get_email
import yaml

class NoSuchSection(Exception):
    def __init__(self, section):
        self.args = (section)

class _Config(object):
    def __init__(self):
        with open('./btslink.yaml') as y:
            self.__data = yaml.load(y)

    def get(self, section, option):
        if section in self.__data and option in self.__data[section]:
            return self.__data[section][option]
        if option in self.__data['general']:
            return self.__data['general'][option]
        return None

    def spool(self):
        return self.__data['general']['spool']

    def cookies(self):
        return self.__data['general']['cookies']

    def ldap(self):
        return self.__data['general']['ldap']

    def resources(self):
        """returns the supported bugtrackers configurations values as a dict
        with keys the project name and value another dict with that BTS infon"""
        res = {}
        for btstype in self.__data['remotes']:
            for bts in self.__data['remotes'][btstype]:
                remote = self.__data['remotes'][btstype][bts]
                remote['type'] = btstype
                remote['uri'] = remote['uri'].rstrip('/')
                if 'uri-re' in remote:
                    remote['uri-re'] = re.compile(remote['uri-re'])
                if 'closing' in remote:
                    remote['closing'] = [s.strip() for s in
                                         remote['closing'].split(',')]
                res[bts] = remote
        return res

    def sender(self):
        if 'from' in self.__data['general']:
            return self.__data['general']['from']
        else:
            return get_email()[1]

    def replyTo(self):
        return self.__data['general']['reply-to']

class _Opener(urllib.FancyURLopener):
    version = "btslink"

    def __init__(self):
        urllib.FancyURLopener.__init__(self)
        self.addheader('Accept', '*/*')

BTSLConfig = _Config()
urllib._urlopener = _Opener()

