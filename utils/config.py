# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import os, re, stat, time, urllib
from sender import get_email
from ConfigParser import RawConfigParser

class NoSuchSection(Exception):
    def __init__(self, section):
        self.args = (section)

class _Config(RawConfigParser):
    def __init__(self):
        RawConfigParser.__init__(self)
        self._dotdir = os.path.expanduser('~/.btslink')
        if not os.path.exists(self._dotdir): os.mkdir(self._dotdir)
        self.read(['/etc/btslink.cfg', './btslink.cfg', self.dotfile('btslink.cfg')])

    def dotfile(self, *args):
        return os.path.join(self._dotdir, *args)


    def get(self, section, option):
        if section and self.has_option(section, option):
            return RawConfigParser.get(self, section, option)
        if self.has_option('general', option):
            return RawConfigParser.get(self, 'general', option)
        return None

    def spool(self):
        return self.get('general', 'spool')

    def cookies(self):
        return self.get('general', 'cookies')

    def ldap(self):
        return self.get('general', 'ldap')

    def resources(self):
        res = {}
        for k in self.sections():
            if k == 'general': continue
            if self.has_option(k, 'type') and self.has_option(k, 'uri'):
                res[k] = {}
                for o, v in self.items(k):
                    if o == 'closing':
                        res[k][o] = [s.strip() for s in v.split(',')]
                    elif o == 'uri':
                        res[k][o] = v.rstrip('/')
                    elif o == 'uri-re':
                        res[k][o] = re.compile(v)
                    else:
                        res[k][o] = v
        return res

    def sender(self):
        tmp = self.get('general', 'from')
        if tmp is None: return get_email()[1]
        return tmp

    def replyTo(self):
        return self.get('general', 'reply-to')

class _Opener(urllib.FancyURLopener):
    version = "btslink"

    def __init__(self):
        urllib.FancyURLopener.__init__(self)
        self.addheader('Accept', '*/*')

BTSLConfig = _Config()
urllib._urlopener = _Opener()

