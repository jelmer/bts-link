# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import sys, os, re, time, traceback

makeToken_re = re.compile(r"[^a-zA-Z0-9@.+\-]")

class ParseExn(Exception):
    def __init__(self, url, reason = None):
        self.url    = url
        self.reason = reason

    def __str__(self):
        return  "Parse error: [%s] %s" % (self.url, self.reason or "")


class DupeExn(Exception):
    def __init__(self, url):
        self.url = url

    def __str__(self):
        return  "Does not deals dupes: [%s]" % (self.url)


def maketoken(val):
    if val and val.lower() == 'none': return None
    return (val and makeToken_re.sub('-', val)) or None


def warn(s):
    print >> sys.stderr, s


def die(s):
    print >> sys.stderr, s
    os.kill(os.getpid(), signal.SIGKILL)
    sys.exit(1)


def getActions(btsbug, rbug):
    add  = []
    rem  = []
    uadd = []
    urem = []

    id            = btsbug.id
    oldStatus     = btsbug.remoteStatus()
    oldResolution = btsbug.remoteResolution()
    wasClosed     = rbug.bts.isClosing(oldStatus, oldResolution)
    wasWontfix    = rbug.bts.isWontfix(oldStatus, oldResolution)

    do = []
    do.append("# remote status report for #%s" % (id))
    do.append("#  * %s" % (rbug.uri))
    # test if remote status changed
    tmp = rbug.status
    if tmp != oldStatus:
        do.append("#  * remote status changed: %s -> %s" % (oldStatus or "(?)", tmp))
        if tmp:
            uadd.append("status-%s" % tmp)
        if oldStatus:
            urem.append("status-%s" % oldStatus)

    # test if remote resolution changed
    tmp = rbug.resolution
    if tmp != oldResolution:
        do.append("#  * remote resolution changed: %s -> %s" % (oldResolution or "(?)", tmp or "(?)"))
        if tmp:
            uadd.append("resolution-%s" % tmp)
        if oldResolution:
            urem.append("resolution-%s" % oldResolution)

    # look if we have to change the 'fixed-upstream' tag
    if rbug.closed != wasClosed:
        if rbug.closed:
            if 'fixed-upstream' not in btsbug.tags:
                do.append("#  * closed upstream")
                add.append('fixed-upstream')
        else:
            if 'fixed-upstream' in btsbug.tags:
                do.append("#  * reopen upstream")
                rem.append('fixed-upstream')

    # look if we have to change the 'wontfix' tag
    if rbug.wontfix != wasWontfix:
        if rbug.wontfix:
            if 'wontfix' not in btsbug.tags:
                do.append("#  * upstream said bug is wontfix")
                add.append('upstream')
                add.append('wontfix')
        else:
            if 'wontfix' in btsbug.tags:
                do.append("#  * upstream said bug isn't wontfix anymore")
                rem.append('wontfix')

    # build mail
    res = []

    if not rbug.isForwardOK(btsbug):
        res.append("forwarded %s %s" % (id, rbug.getForward(btsbug)))
    if rem:
        res.append("tags %s - %s" % (id, ' '.join(rem)))
    if add:
        res.append("tags %s + %s" % (id, ' '.join(add)))
    if urem:
        res.append("usertags %s - %s" % (id, ' '.join(urem)))
    if uadd:
        res.append("usertags %s + %s" % (id, ' '.join(uadd)))

    if res:
        res = do + res + ['']

    return res


class RemoteReport:
    """Describes a bug report that exists in a remote bug tracker."""
    def __init__(self, data, bts):
        self.bts = bts

        assert(data.id and data.status)

        self.id         = data.id
        self.status     = maketoken(data.status)
        self.resolution = maketoken(data.resolution)
        self.uri        = self.bts.getUri(self.id)

        self.closed     = self.bts.isClosing(self.status, self.resolution)
        self.wontfix    = self.bts.isWontfix(self.status, self.resolution)

    def isForwardOK(self, btsbug):
        id = self.bts.extractBugid(btsbug.forward)
        if id == self.id:
            return btsbug.merge is None

        if btsbug.merge:
            id = self.bts.extractBugid(btsbug.merge)
            return id == self.id

        return False

    def getForward(self, btsbug):
        id = self.bts.extractBugid(btsbug.forward)
        if id == self.id:
            return self.uri
        return "%s, merged-upstream: %s" % (btsbug.forward, self.uri)


class RemoteBts:
    """A remote bug tracker."""
    resources = {}
    _remotes  = {}

    ##########################################################################
    # static methods

    def setup(cls, res):
        for k, v in res.iteritems():
            typ = v['type']
            if typ not in cls._remotes:
                print >> sys.stderr, "`%s' is an unknown remote BTS type" % (typ)
                sys.exit(1)
            v['bts'] = cls._remotes[typ](v)
            cls.resources[k] = v
    setup = classmethod(setup)

    def register(cls, typ, thecls):
        cls._remotes[typ] = thecls
    register = classmethod(register)

    def find(cls, uri):
        for k, v in cls.resources.iteritems():
            if 'uri-re' in v:
                if v['uri-re'].match(uri):
                    return v['bts']
            elif uri.startswith(v['uri']):
                return v['bts']
        return None
    find = classmethod(find)

    ##########################################################################
    # public methods

    def __init__(self, cnf, extractRe = None, uriFmt = None, bugCls = None):
        self._cnf    = cnf
        self._bugre  = extractRe and re.compile(extractRe % cnf)
        self._urifmt = uriFmt
        self._bugcls = bugCls
        self._queue  = []

    def enqueue(self, btsbug):
        self._queue.append(btsbug)

    def processQueue(self):
        res = []
        while self._queue:
            btsbug      = self._queue[0]
            self._queue = self._queue[1:]

            try:
                rbug = self.getReport(btsbug.forward)
                if rbug:
                    actions = getActions(btsbug, rbug)
                    if actions:
                        res.append((btsbug, actions))
            except KeyboardInterrupt:
                die("*** ^C...")
            except ParseExn, e: warn(str(e))
            except DupeExn, e:  warn(str(e))
            except:
                i = 0
                type, value, tb = sys.exc_info()
                exn = traceback.format_exception_only(type, value)
                warn("*** #%s [%s] raised an exception %s" % (btsbug.id, btsbug.forward, exn))
                for file, lineno, _, text in traceback.extract_tb(tb):
                    i += 1
                    warn(" %d. %-70s [%s:%s]" % (i, text, os.path.basename(file), lineno))

            if not self._queue:
                break

            time.sleep(3)
        return res

    def getReport(self, uri):
        data = self._getReportData(uri)
        return data and RemoteReport(data, self)

    def extractBugid(self, uri):
        if 'uri-re' in self._cnf:
            m = self._cnf['uri-re'].match(uri)
            return m and m.group(1)
        else:
            return self._extractBugid(uri)

    def getUri(self, bugId):
        if 'bugfmt' in self._cnf:
            return self._cnf['bugfmt'] % (bugId)
        return self._getUri(bugId)

    ##########################################################################
    # public virtuals

    def isClosing(self, status, resolution): pass
    def isWontfix(self, status, resolution): pass

    ##########################################################################
    # protected virtual methods

    def _extractBugid(self, uri):
        assert self._bugre
        m = self._bugre.match(uri)
        return m and m.group(1)

    def _getUri(self, bugId):
        assert self._urifmt
        return self._urifmt % {'uri': self._cnf['uri'], 'id': bugId}

    def _getReportData(self, uri):
        assert self._bugcls
        return self._bugcls(uri, self.extractBugid(uri))

    def _bugId(self, data):
        return data.id

import berlios, bugzilla, gnats, launchpad, mantis, rt, savane, sourceforge, trac

