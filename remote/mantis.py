# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import urllib, urlparse, cgi, re

from BeautifulSoup import BeautifulSoup
from __init__ import *
from base import maketoken

status_re = re.compile(r'^\s*Status\s*$')
resolution_re = re.compile(r'^\s*Resolution\s*$')

def parse_table(soup, key):
    cell = soup.firstText(key).findParent('td') or None
    return cell and maketoken(cell.findNextSibling('td').string.strip())

def find_dupe(soup):
    cell = soup.firstText('duplicate of').findParent('td')
    cell = cell.findNextSibling('td')
    return cell.a.string

class MantisData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Mantis: no id")

        soup = BeautifulSoup(urllib.urlopen(uri))

        self.status     = parse_table(soup, status_re) or failwith(uri, "Mantis: no status")
        self.resolution = parse_table(soup, resolution_re)

        if self.resolution == 'duplicate':
            self.duplicate = find_dupe(soup) or failwith(uri, "Mantis: cannot find duplicate")

class RemoteMantis(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/(?:view.php\?id|bug_view_(?:advanced_)?page.php\?bug_id)=([0-9]+)$'
        urifmt = '%(uri)s/view.php?id=%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, MantisData)

    def isClosing(self, status, resolution):
        return status in ('resolved', 'closed') and not self.isWontfix(status, resolution)

    def isWontfix(self, status, resolution):
        return resolution in ('won-t-fix', 'not-fixable')


    def _getReportData(self, uri):
        id = self.extractBugid(uri)
        if not id: return None

        data = MantisData(self.getUri(id), id)

        while data.resolution == 'duplicate':
            data = MantisData(self.getUri(data.duplicate), data.duplicate)

        return data

RemoteBts.register('mantis', RemoteMantis)
