# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2008 Jelmer Vernooij <jelmer@samba.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import urllib, urlparse, re

from __init__ import *
import rfc822


class LaunchpadTask:
    def __init__(self, msg):
        self.product = msg["task"]
        self.status = msg["status"]
        self.assignee = msg["assignee"]
        self.reporter = msg["reporter"]


class LaunchpadBug:
    def __init__(self, text):
        bug = rfc822.Message(text)
        dupestr = bug.get("duplicate-of", None)
        if dupestr is None or dupestr == "":
            self.duplicate_of = None
        else:
            self.duplicate_of = int(dupestr)
        self.id = int(bug["bug"])
        self.tasks = {}
        self.title = bug["title"]
        self.reporter = bug["reporter"]

        # Find the task related to the product we're dealing 
        # with at the moment
        task = rfc822.Message(text)
        while "task" in task:
            self.tasks[task["task"]] = LaunchpadTask(task)
            task = rfc822.Message(text)


class RemoteLaunchpadData:
    def __init__(self, uri, id=None):
        text = urllib.urlopen(urlparse.urljoin(uri+"/", "+text"))
        # Two forms of URLs supported:
        if uri.split("/")[-2] == "+bug":
            # https://bugs.launchpad.net/bzr/+bug/XXXXXX
            product = uri.split("/")[-3]
        else:
            # https://bugs.launchpad.net/bugs/XXXXXX
            product = None
        self.resolution = None

        lp_bug = LaunchpadBug(text)
        if id is not None and int(id) != lp_bug.id:
            raise ParseExn(uri, "Expected id %s, got %d" % (id, lp_bug.id))
        self.id = lp_bug.id

        if lp_bug.duplicate_of is not None:
            dupe_uri = "%s/%d" % ("/".join(uri.split("/")[:-1]), 
                                  lp_bug.duplicate_of)
            self.duplicate = RemoteLaunchpadData(dupe_uri)
        else:
            self.duplicate = None

        if product is None: 
            if len(lp_bug.tasks) != 1:
                raise ParseExn(uri, 
                    "No product specified but bug affects multiple products")
            else:
                self.status = lp_bug.tasks[lp_bugs.keys()[0]].status

        if product not in lp_bug.tasks:
            raise ParseExn(uri, "No task affecting product %s" % product)

        self.status = lp_bug.tasks[product].status


class RemoteLaunchpad(RemoteBts):
    def __init__(self, cnf):
        bugre  = r"^%(uri)s/(?:.*/)?\+bug/([0-9]+)$"
        urifmt = "%(uri)s/bugs/%(id)s"
        RemoteBts.__init__(self, cnf, bugre, urifmt, RemoteLaunchpadData)

    def isClosing(self, status, resolution):
        return status == "Fix-Released" or status == "Fix-Committed"

    def isWontfix(self, status, resolution):
        return status == "Won-t-Fix" or status == "Invalid"

RemoteBts.register('launchpad', RemoteLaunchpad)

