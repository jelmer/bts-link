# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Sanghyeon Seo   <sanxiyn@gmail.com>
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import urllib, urlparse, cgi

from BeautifulSoup import BeautifulSoup
from __init__ import *
from base import maketoken

def parse_table(soup, key):
    cell  = soup.firstText(key).findParent('td')
    return maketoken(cell.nextSibling.string)

class SavaneData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Savane: no id")

        soup = BeautifulSoup(urllib.urlopen(uri))

        self.status     = parse_table(soup, 'Open/Closed:') or failwith(uri, "Savane: no status")
        self.resolution = parse_table(soup, 'Status:')

        if self.resolution == 'Duplicate':
            raise DupeExn(uri)

class RemoteSavane(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/\?func=detailitem&item_id=([0-9]+)$'
        urifmt = '%(uri)s/?func=detailitem&item_id=%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, SavaneData)

    def isClosing(self, status, resolution):
        return status == 'Closed' and resolution != 'Wont-Fix'

    def isWontfix(self, status, resolution):
        return resolution == 'Wont-Fix'

RemoteBts.register('savane', RemoteSavane)

