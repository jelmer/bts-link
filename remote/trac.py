# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Sanghyeon Seo   <sanxiyn@gmail.com>
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

# @see http://projects.edgewall.com/trac/wiki/TracTickets

import re, urllib

from BeautifulSoup import BeautifulSoup
from __init__ import *

any = re.compile(r'.*')


class TracTicket:
    def __init__(self, page):
        lines = page.split('\n');

        if len(lines) == 2:
            keys = lines[0].split('\t')
            vals = lines[1].split('\t')
            dct  = dict((keys[i], vals[i]) for i in range(0, len(keys)))

            self.status     = dct['status']
            self.resolution = dct['resolution']
            self.id         = int(dct['id'])
            self.reporter   = dct['reporter']
            self.owner      = dct['owner']
            self.summary    = dct['summary']
            self.milestone  = dct['milestone']
            self.version    = dct['version']
            self.priority   = dct['priority']
            self.description = dct['description'].replace("\\r\\n", "\r\n")
        else:
            soup = BeautifulSoup(page)

            status = soup.first(any, 'status')
            if status:
                self.status, self.resolution = self._process_status(status.strong.string)
            else:
                try:
                    # Trac 0.8.x
                    self.status = soup.first(any, dict(headers='h_status')).string
                    resolution  = soup.first(any, dict(headers='h_resolution')).string
                    self.resolution = (resolution != '&nbsp;' and resolution) or None
                except:
                    failwith(uri, "Probably error page")

    def _process_status(self, text):
        words = text.split()
        assert len(words) in (1, 2), "invalid status/resolution: %r" % words
        if len(words) == 1:
            return text, None
        else:
            return [x.strip('()') for x in words]


class TracData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Trac: no id")

        page = wget(uri + '?format=tab')

        ticket = TracTicket(page)

        self.status = ticket.status
        self.resolution = ticket.resolution

        if not ticket.status:
            failwith(uri, "Trac: no status")

        if ticket.resolution == "duplicate":
            raise DupeExn(uri)


class RemoteTrac(RemoteBts):
    def __init__(self, cnf):
        bugre  =  r"^%(uri)s/ticket/([0-9]+)$"
        urifmt = "%(uri)s/ticket/%(id)s"
        RemoteBts.__init__(self, cnf, bugre, urifmt, TracData)

    def isClosing(self, status, resolution):
        return status == 'closed' and resolution != 'wontfix'

    def isWontfix(self, status, resolution):
        return resolution == 'wontfix'

RemoteBts.register('trac', RemoteTrac)
