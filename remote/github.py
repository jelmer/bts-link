# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   � 2012 Sandro Tosi <morph@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

# @see http://developer.github.com/v3/issues/

import urllib2, urlparse, cgi, re, json, ssl

from BeautifulSoup import BeautifulSoup
from __init__ import *
from secrets import SECRETS

TOKEN = SECRETS['github']

class GithubData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Github: no id")

        context = ssl.create_default_context(capath=CAPATH)

        # from the "visual" URL get the api URL
        uri = uri.replace('https://github.com', 'https://api.github.com/repos')
        # needed to support querying pull requests too
        uri = uri.replace('/pull/', '/pulls/')
        req = urllib2.Request(uri, headers = { 'Authorization': 'token %s' % TOKEN })
        data = json.load(urllib2.urlopen(req, context=context))

        self.status = data['state'] or failwith(uri, "Github", exn=NoStatusExn)
        self.resolution = None

class RemoteGithub(RemoteBts):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, GithubData)

    # override base class method to extract the ID as useful as for _getUri
    def extractBugid(self, uri):
        bugre = re.compile(r"https://github.com/(?P<user>.*)/(?P<project>.*)/(?P<type>.*)/(?P<id>[0-9]+)(?P<comment>#issuecomment-[0-9]+)?$")
        res = bugre.match(uri)
        if res:
            return res.groupdict()
        else:
            return None

    # return a meaningful uri
    def _getUri(self, bugId):
        return "https://github.com/%(user)s/%(project)s/%(type)s/%(id)s" % bugId

    def isClosing(self, status, resolution):
        return status in ('closed',)

RemoteBts.register('github', RemoteGithub)
