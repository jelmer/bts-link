# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import urlparse, cgi, commands

from BeautifulSoup import BeautifulSoup
from __init__ import *
from base import maketoken

def get_value(table, key):
    td = table.firstText(key).findParent('td')
    return td.contents[-1].string.strip()

class SourceForgeData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "SourceForge: no id")

        soup  = BeautifulSoup(wget(uri))
        table = soup.first('table', {'cellpadding': '0', 'width': '100%'})

        self.status     = get_value(soup, 'Status: ') or failwith(uri, "SourceForge: no status")
        self.resolution = get_value(soup, 'Resolution: ') or None

        if self.resolution == 'Duplicate':
            raise DupeExn(uri)

class RemoteSourceForge(RemoteBts):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, SourceForgeData)

    def _getUri(self, bugId):
        k = ['atid', 'aid', 'group_id']
        qs = ['%s=%s' % (x, bugId[x]) for x in k]
        return "http://sourceforge.net/tracker/?func=detail&" + '&'.join(qs)

    def extractBugid(self, uri):
        query = cgi.parse_qs(urlparse.urlparse(uri)[4])
        id = {}
        for k in 'atid', 'aid', 'group_id':
            if not k in query: failwith(uri, "SourceForge: %s key is missing" % k)
            id[k] = query[k][0]
        return id

    def isClosing(self, status, resolution):
        return status == 'Closed' and resolution != 'Wont-Fix'

    def isWontfix(self, status, resolution):
        return resolution == 'Wont-Fix'

RemoteBts.register('sourceforge', RemoteSourceForge)


