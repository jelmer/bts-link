# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2009 Ryan Niebur <ryanryan52@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

import urllib, urlparse, cgi, ssl

from BeautifulSoup import BeautifulSoup
from __init__ import *

def parse_table(soup):
    return soup.first('td', attrs={'headers' : 'status'}).string.strip()

class FlySprayData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "FlySpray: no id")

        context = ssl.create_default_context(capath=CAPATH)
        soup = BeautifulSoup(urllib.urlopen(uri, context=context))

        self.status = parse_table(soup) or failwith(uri, "FlySpray", exn=NoStatusExn)
        closeddiv = soup.first('div', attrs={'id' : 'taskclosed'})
        if closeddiv:
            self.resolution = closeddiv.contents[7].strip()[6:]
        else:
            self.resolution = None

        if self.resolution == 'Duplicate':
            raise DupeExn(uri)

class RemoteFlySpray(RemoteBts):
    def __init__(self, cnf):

        bugre  = r"^%(uri)s/index.php?(?:.*&)?task_id=([0-9]+)(?:&.*)?$"
        urifmt = "%(uri)s/index.php?do=details&task_id=%(id)s"

        RemoteBts.__init__(self, cnf, bugre, urifmt, FlySprayData)

    def isClosing(self, status, resolution):
        return status == 'Closed' and resolution == 'Fixed'

    def isWontfix(self, status, resolution):
        return status == "Incomplete" or (status == 'Closed' and resolution in ('Will-Not-Implement', 'Invalid', 'Works-for-me'))

RemoteBts.register('flyspray', RemoteFlySpray)
