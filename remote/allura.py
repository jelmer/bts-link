# vim:set encoding=utf-8:
###############################################################################
# Copyright:
#   © 2006 Pierre Habouzit <madcoder@debian.org>
#     2015 Olivier Berger <olivier.berger@telecom-sudparis.eu>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of its contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
###############################################################################

# Allura trackers support (Apache Allura : https://allura.apache.org/)

from __init__ import *
import re, urllib2, json, string, ssl
        
class AlluraTrackerData:
    def __init__(self, uri, urlcomponents):
        self.id = urlcomponents or failwith(uri, "Allura: no id : %s" % str(urlcomponents))

        status = None
        resolution = None

        # We access the REST API to fetch JSON description of the bug
        apiurl = "%(uri)s/rest/p/%(project)s/%(type)s/%(id)s/" % urlcomponents

        context = ssl.create_default_context(capath=CAPATH)
        req = urllib2.Request(apiurl)
        data = json.load(urllib2.urlopen(req, context=context))
        
        ticketdata = data.get('ticket')
        if data:
            status = ticketdata.get('status')

        self.status = status or failwith(uri, "Allura", exn=NoStatusExn)
        
        # I hope the status are more or less constant among projects, but not sure
        if status == 'closed-duplicate':
            resolution = 'duplicate'
        
        self.resolution = resolution
        
        if self.resolution == 'duplicate':
            raise DupeExn(uri)
        

class RemoteAlluraTracker(RemoteBts):
    '''
    Support for Allura bug trackers
    '''
    def __init__(self, cnf):
        
        RemoteBts.__init__(self, cnf, None, None, AlluraTrackerData)

    # override base class method to extract the URL components as useful as for _getUri
    def extractBugid(self, uri):
        bugre = re.compile(r"^(?P<uri>.*)/p/(?P<project>[^/]+)/(?P<type>[^/]+)/(?P<id>[0-9]+)/?$")
        res = bugre.match(uri)
        if res:
            return res.groupdict()
        else:
            return None

    # return a meaningful uri
    def _getUri(self, urlcomponents):
        return "%(uri)s/p/%(project)s/%(type)s/%(id)s/" % urlcomponents
    
    def isClosing(self, status, resolution):
        # All status starting by 'closed' are matched by default, unless otherwise specified in the 'closing' conf value
        if not status:
            return False
        else:
            if 'closing' in self._cnf:
                return status in self._cnf['closing']
            else: 
                return (string.find(status, 'closed') == 0)
        
    def isWontfix(self, status, resolution):
        # Again I hope the status are more or less constant among projects, but not sure
        return status and (string.find(status, "closed-wont-fix") == 0)

RemoteBts.register('allura', RemoteAlluraTracker)
