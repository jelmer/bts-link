#! /bin/sh

SPOOL=../db-h

exec `dirname $0`/../btspull -n $@ \
     `grep http $SPOOL/index.fwd        \
         | sort -n | cut -d: -f1 | tr '\n' ' '`

