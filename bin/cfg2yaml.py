#!/usr/bin/python
#
# Convert bts-link config file from cfg to yaml format

from ConfigParser import RawConfigParser
from collections import defaultdict
import yaml

cfg = RawConfigParser()
cfg.read('./btslink.cfg')

yamlcfg = defaultdict(dict)
remotes = defaultdict(lambda: defaultdict(dict))

for i in ('user', 'reply-to', 'spool', 'cookies', 'ldap'):
    yamlcfg['general'][i] = cfg.get('general', i)

cfg.remove_section('general')

for i in 'local', 'btsnode':
    yamlcfg[i]['logdir'] = cfg.get(i, 'logdir')
    cfg.remove_section(i)

# now only remotes are left

for remote in sorted(cfg.sections()):
    type = cfg.get(remote, 'type')
    for option in [x for x in cfg.options(remote) if x != 'type']:
        remotes[type][remote][option] = cfg.get(remote, option)

# force cast to dict, so yaml dumper won't include py objects types in output
for d in remotes:
    remotes[d] = dict(remotes[d])

yamlcfg['remotes'] = dict(remotes)
print yaml.dump(dict(yamlcfg), default_flow_style=False)

