#! /bin/bash

set -e

export LC_ALL=C

opts=q

spool="$1"
if [ "x$1" = "x-v" ]; then
    opts=v
    spool="$2"
fi

index=index.fwd
[ -n "$spool" -a -d "$spool" ] || exit 1

rsync -ad$opts rsync://bugs-mirror.debian.org/bts-spool-db/ \
    --include '*/*.summary' --exclude '*/*' --exclude '???*' $spool

rsync -ad$opts rsync://bugs-mirror.debian.org/bts-spool-index/sources $spool/sources

pushd $spool &>/dev/null
grep -r '^Forwarded-To' . | sed -e 's~^\./../\([0-9]*\).summary:Forwarded-To:~\1:~' > $index.new
mv $index.new $index
popd &>/dev/null
